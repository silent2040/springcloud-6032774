package com.gientech.elasticsearch.controller;

import com.gientech.elasticsearch.entity.User;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;

@RestController
public class UserController {

    @Resource
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @GetMapping("/save")
    public String save(){
        User user = new User();
        user.setId("120");
        user.setName("小何");
        user.setAge(29);
        user.setSex("Man");
        user.setPhone("18723132040");

        // 新增
        elasticsearchRestTemplate.save(user);

        //indexName动态制定--索引如果不存在save会自动创建后再存入数据
        elasticsearchRestTemplate.save(user, IndexCoordinates.of("test_user_info"));

        elasticsearchRestTemplate.save(user, IndexCoordinates.of("test_user_info_dd"));

        return "success";
    }

    @GetMapping("/query")
    public Object search(){
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
        builder.withQuery(queryBuilder);
        NativeSearchQuery build = builder.build();

        SearchHits<User> user = elasticsearchRestTemplate.search(build , User.class);

        //indexName动态指定
        SearchHits<User> user_info = elasticsearchRestTemplate.search(build , User.class ,IndexCoordinates.of("test_user_info"));

        HashMap<Object, Object> objectHashMap = new HashMap<Object, Object>(5);
        objectHashMap.put("test_user", user.getSearchHits());
        objectHashMap.put("test_user_info", user_info.getSearchHits());

        return objectHashMap;
    }

    @GetMapping("/delete")
    public String delete(){

        //根据ID删除-indexName默认取@Document中indexName
        elasticsearchRestTemplate.delete("120", User.class);

        //根据ID删除-indexName动态指定
        elasticsearchRestTemplate.delete("120", IndexCoordinates.of("test_user_info_dd"));

        return "delete succ";
    }
}
