package com.gientech.consulprovider.contrller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProviderController {
    @GetMapping("provider-test")
    public String test(){

        return "pro-test";
    }
}
