package com.gientech.consulconsumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@RestController
public class ConsumerController {
    @Autowired
    private RestTemplate restTemplate;

    /**discovery 客户端实列**/
    @Autowired
    private DiscoveryClient discoveryClient;
    @GetMapping("consumer-test")
    public String test(){
        String str = null;
        // 通过consul去获取provider的服务
//        str = restTemplate.getForObject("http://localhost:8001/provider-test", String.class);

        // 通过discovery获取instance
        List<ServiceInstance> provider = this.discoveryClient.getInstances("provider");
        ServiceInstance serviceInstance = provider.get(0);
        URI uri = serviceInstance.getUri();
        // 拼接访问路径
        String url = uri+"/provider-test";
        str = restTemplate.getForObject(url, String.class);
        return "consul consumer test:"+str;
    }
    @GetMapping("test1")
    public String test1(){
        return "123132";
    }
}
