package com.gientech.vueprovider.controller;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class VueRegister {
    @Resource
    StringRedisTemplate stringRedisTemplate;
    @GetMapping("/register")
    public boolean vueRegister(@RequestParam(value = "username") String username, @RequestParam(value = "password") String password){

        System.out.println("username:"+username + "   password:"+password);
        boolean result = false;
        try{
            // 用户信息存储在redis中
            stringRedisTemplate.opsForValue().set("username", username);
            stringRedisTemplate.opsForValue().set("password", password);
            // 存储完成更新结果
            result = true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }
}
