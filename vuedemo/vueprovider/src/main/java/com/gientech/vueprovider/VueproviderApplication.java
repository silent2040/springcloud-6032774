package com.gientech.vueprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VueproviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(VueproviderApplication.class, args);
	}

}
