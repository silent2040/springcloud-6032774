/* eslint-disable */
// 1. 完成和路由的工作
import { createRouter, createWebHashHistory } from "vue-router";
// 2. 定义一些路由
// 每个路由都需要映射到一个组件
const routes = [
    {
        path: '/main',
        name: 'main',
        component: () => import('@/components/main')
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('@/components/login')
    },
    {
        path: '/register',
        name: 'register',
        component: () => import('@/components/register')
    }
]

// 3. 创建路由实列并传递 routes 配置
const router = createRouter({
    // 4. 内部提供了 history 模式的实现，为了简单起见我们使用hash模式
    history: createWebHashHistory(),
    // routes: routes 的缩写
    routes,
})

export default router;