/* eslint-disable */
import { createApp } from 'vue'
import App from './App.vue'
// import axios from 'axios'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import '@/css/common.css'
import router from './router/index.js'


let app = createApp(App)
// app.use(axios)
app.use(ElementPlus)
app.use(router)
app.mount('#app')
