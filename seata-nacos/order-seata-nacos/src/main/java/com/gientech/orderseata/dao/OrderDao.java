package com.gientech.orderseata.dao;

import com.gientech.orderseata.pojo.Order;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper  // 告诉springboot这是一个mybatis的mapper
@Repository // 将OrderDao交由spring容器管理
public interface OrderDao {

    public int addOrder(Order order);



}
