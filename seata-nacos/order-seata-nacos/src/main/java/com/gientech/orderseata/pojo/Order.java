package com.gientech.orderseata.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {

    private Integer id;

    private Integer productId;

    private Integer totalAmount;

    private Integer status;


}
