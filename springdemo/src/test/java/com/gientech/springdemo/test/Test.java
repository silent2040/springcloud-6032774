package com.gientech.springdemo.test;

import com.alibaba.fastjson.JSONObject;
import com.gientech.springdemo.dto.User;

public class Test {
    public static void main(String[] args) {
        User user = new User("5002421993041222","何伟", "男", 30, "18888888888");

        String jsonStr = JSONObject.toJSONString(user);

        System.out.println(jsonStr);

        User user1 = JSONObject.parseObject(jsonStr, User.class);

        System.out.println(user1.getUserId());
    }
}
