package com.gientech.springdemo.dto;

public class User {
    /**用户id**/
    private String userId;
    /**用户姓名**/
    private String name;
    /**用户性别**/
    private String sex;
    /**用户年龄**/
    private Integer age;
    /**用户手机号**/
    private String telPhone;

    public User(String userId, String name, String sex, Integer age, String telPhone) {
        this.userId = userId;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.telPhone = telPhone;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getTelPhone() {
        return telPhone;
    }

    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone;
    }
}
