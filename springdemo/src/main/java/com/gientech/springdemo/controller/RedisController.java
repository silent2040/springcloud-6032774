package com.gientech.springdemo.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RestController
public class RedisController {

    private final static String REDIS_LOCK_KEY = "LOCK123";
    private final static Long REDIS_KEY_EXP = 50l;
    @Resource
    StringRedisTemplate stringRedisTemplate;
    @Resource
    RedisTemplate redisTemplate;
    @GetMapping("redis/set")
    public String redisSet(){
        String key = "unitName";
        String value = "gientech";

        stringRedisTemplate.opsForValue().set(key, value);
        return "set success";
    }
    @GetMapping("redis/get")
    public String redisGet(){
        return stringRedisTemplate.opsForValue().get("unitName");
    }

    /**
     * 实现分布式锁 1.提现竞争 2.超时释放
     * @return
     */
    @GetMapping("redis/lock")
    public String redisLock(){
        // 1. 定义反馈信息
        String returnMessage;
        // 2. 获取定义锁名称
        String lockKey = stringRedisTemplate.opsForValue().get(REDIS_LOCK_KEY);
        // 3. 判断key对应value是否存在，若存在表示已上锁，不可以进行业务处理；若不存在表示未上锁，可以进行业务处理
        if (StringUtils.isBlank(lockKey)){
            // 4. 上锁操作，设置超时时间
            stringRedisTemplate.opsForValue().set(REDIS_LOCK_KEY,"123",REDIS_KEY_EXP, TimeUnit.SECONDS);
            returnMessage = "success";
        }else {
            // 5. 若达到超时时间仍为释放，则需强制释放
            Long expire = stringRedisTemplate.getExpire(REDIS_LOCK_KEY, TimeUnit.SECONDS);
            if (expire > 0) {
                returnMessage = "对不起！系统繁忙，请稍后重试！";
            }else {
                // 是否存在此种场景？
//                String andPersist = stringRedisTemplate.opsForValue().getAndPersist(REDIS_LOCK_KEY);
                returnMessage = "对不起！系统繁忙，请稍后重试！";
            }

        }
        return returnMessage;
    }

}
