package com.gientech.springdemo.controller;

import com.alibaba.fastjson.JSONObject;
import com.gientech.springdemo.dto.User;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.util.List;

@RestController
public class ConsumerController {

    /**定义redis**/
    @Resource
    StringRedisTemplate stringRedisTemplate;
    @GetMapping("/mq/getMessage")
    public void getMessage() throws Exception{
        // 1.定义消费者
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("consumer");
        // 2. 设置nameserver
        consumer.setNamesrvAddr("127.0.0.1:9876");
        // 3. 订阅主题topic
        consumer.subscribe("heweiTest","*");
        // 4. 处理broker拉取回来消息
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                System.out.printf("%s receive new message: %s %n",Thread.currentThread().getName(), list);

                System.out.println("-----------");
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        consumer.start();
        System.out.println("consumer start");
    }

    /**
     * 消费新用户信息，并登记到redis
     * @return
     */
    @GetMapping("rocketMq/consumer/start")
    public void consumerListerStart() throws Exception{
        // 1. 实例化消费者
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("consumer");
        // 2. 设置nameServer 订阅主题
        consumer.setNamesrvAddr("127.0.0.1:9876");
        consumer.subscribe("heWeiTest","*");
        // 3. 设置模式
//        consumer.setMessageModel(MessageModel.BROADCASTING);

        //4. 消费者 消息监听
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                // 5. 监听到消息进行消费
                msgs.forEach(messageExt -> {
                    // 6. 消息登记到redis中
                    try {
                        this.regUserInfoToRedis(messageExt);
                    } catch (UnsupportedEncodingException e) {
                        throw new RuntimeException(e);
                    }
                });

                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }

            private void regUserInfoToRedis(MessageExt messageExt) throws UnsupportedEncodingException {
                // 6.1 获取消息体
                byte[] extBody = messageExt.getBody();
                String bodyStr = new String(extBody,"UTF-8") ;
                // 6.2 json转换为java对象
                User user = JSONObject.parseObject(bodyStr, User.class);
                // 6.3 将user的id作为key值存放
                String key = stringRedisTemplate.opsForValue().get(user.getUserId());
                // 6.4 判断key对应值是否存在，若有值表示已登记，无需再次登记，若无值则登记
                if (StringUtils.isBlank(key)) {
                    stringRedisTemplate.opsForValue().set(user.getUserId(), bodyStr);
                }
            }

        });

        // 7. 启动消费者
        consumer.start();
    }

    @GetMapping("rocketMq/consumer/get")
    public String getUserInfo(){

        return stringRedisTemplate.opsForValue().get("5002421993041222");
    }
}
