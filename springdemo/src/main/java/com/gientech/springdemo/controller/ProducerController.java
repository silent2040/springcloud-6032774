package com.gientech.springdemo.controller;

import com.alibaba.fastjson.JSONObject;
import com.gientech.springdemo.dto.User;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ProducerController {
    @GetMapping("/mq/setMessage")
    public void syncMethodOne() throws Exception {
        DefaultMQProducer mqProducer = new DefaultMQProducer("provider");
        mqProducer.setNamesrvAddr("127.0.0.1:9876");

        mqProducer.start();

        for (int i = 0; i < 5; i++) {
            //    private String topic;
            //    private int flag;
            //    private Map<String, String> properties;
            //    private byte[] body;
            //    private String transactionId;
            //public Message(String topic, String tags, String keys, byte[] body)
            Message message = new Message("heweiTest", "TagA", "MQ序号"+i+1, "测试".getBytes("UTF-8"));

            mqProducer.send(message);

        }

        mqProducer.shutdown();
    }

    /**
     * provider 发布新用户信息
     */
    @GetMapping("/rocketMq/producer/start")
    public void regNewUserInfo() throws Exception{
        // 1. 生产者实例化
        DefaultMQProducer producer = new DefaultMQProducer("provider");
        // 2. 设置nameServer
        producer.setNamesrvAddr("127.0.0.1:9876");
        // 3. 启动生产者
        producer.start();

        // 4. 获取用户信息 userId唯一
        User user = new User("5002421993041222","何伟", "男", 30, "18888888888");

        String jsonStr = JSONObject.toJSONString(user);

        // 5. 消息发送
        Message message = new Message("heWeiTest", "TagA", user.getUserId(), jsonStr.getBytes());
        producer.send(message);

        // 6. 关闭生产者
        producer.shutdown();

    }
}
