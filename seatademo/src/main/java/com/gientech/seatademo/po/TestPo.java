package com.gientech.seatademo.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestPo {
    private int id;
    private String userId;
    private String userName;

    private String userSts;
}
