package com.gientech.seatademo.mapper;

import com.gientech.seatademo.po.TestPo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface TestMapper {

    /**
     * 新增
     * @param testPo
     * @return
     */
    int addUser(TestPo testPo);
}
