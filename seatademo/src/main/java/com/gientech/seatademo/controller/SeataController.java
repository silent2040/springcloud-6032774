package com.gientech.seatademo.controller;

import com.gientech.seatademo.mapper.TestMapper;
import com.gientech.seatademo.po.TestPo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SeataController {

    @Autowired
    private TestMapper testMapper;

    @GetMapping("/add")
    public String AddUser(){
        // 新增对象
        TestPo testPo = new TestPo();
        testPo.setId(1);
        testPo.setUserId("1234");
        testPo.setUserName("测试");
        testPo.setUserSts("1");

        int addUser = testMapper.addUser(testPo);

        return "this is ok 新增条数:"+addUser;
    }
}
